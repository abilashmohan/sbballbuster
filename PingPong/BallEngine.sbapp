<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<modify version="7.6.3">
    <add>
        <annotations>
            <annotation name="hygienic"/>
        </annotations>
        <import alias="PlayerSessionSchema" from="SharedSchemas.sbint" item="PlayerSessionSchema"/>
        <import alias="PlayerSessionTableSchema" from="SharedSchemas.sbint" item="PlayerSessionTableSchema"/>
        <parameter-definitions>
            <parameter default="100" name="PaddleLength"/>
            <parameter default="false" name="Loopy"/>
        </parameter-definitions>
        <dynamic-variables>
            <dynamic-variable initial-expression="1" name="Level" setter-expression="updateLevel" setter-stream="DV"/>
        </dynamic-variables>
        <data name="BallPosition" shared="false" source="concrete" type="querytable">
            <param name="storage-method" value="memory"/>
            <param name="replicated" value="false"/>
            <table-schema>
                <schema>
                    <field name="ballId" type="int"/>
                    <field name="x" type="int"/>
                    <field name="y" type="int"/>
                    <field name="velocityX" type="int"/>
                    <field name="velocityY" type="int"/>
                </schema>
                <primary-index type="btree">
                    <field name="ballId"/>
                </primary-index>
            </table-schema>
            <param name="truncate" value="false"/>
            <preload format="csv" mode="empty"/>
        </data>
        <data name="CurrentSession" shared="false" source="input" type="querytable">
            <param name="storage-method" value="memory"/>
            <param name="replicated" value="false"/>
            <param name="table-schema" value="PlayerSessionTableSchema"/>
            <param name="truncate" value="false"/>
            <preload format="csv" mode="empty"/>
        </data>
        <stream name="ExternalCommands">
            <schema>
                <field name="Command" type="string"/>
            </schema>
        </stream>
        <stream name="NextLevel">
            <schema>
                <field name="upLevel" type="int"/>
            </schema>
        </stream>
        <box name="Metronome" type="metronome">
            <output port="1" stream="out:Metronome_1"/>
            <param name="interval" value="0.016"/>
            <param name="timestamp-field" value="time"/>
        </box>
        <box name="FetchCurrentDV" type="map">
            <input port="1" stream="NextLevel"/>
            <output port="1" stream="DV"/>
            <target-list>
                <item name="input" selection="none"/>
                <expressions>
                    <include field="updateLevel">upLevel</include>
                </expressions>
            </target-list>
        </box>
        <box name="Filter2" type="filter">
            <input port="1" stream="ExternalCommands"/>
            <output port="1" stream="out:Filter2_1"/>
            <param name="autogen-nomatch-port" value="false"/>
            <param name="expression.0" value="Command == 'ResetBall'"/>
        </box>
        <box name="GetBallPosition" type="query">
            <input port="1" stream="out:Metronome_1"/>
            <output port="1" stream="out:GetBallPosition_1"/>
            <dataref id="querytable" name="BallPosition"/>
            <param name="operation" value="read"/>
            <param name="where" value="primary-key"/>
            <param name="limit" value="1"/>
            <param name="key-field.0" value="ballId"/>
            <param name="key-lowerbound-value.0" value="1"/>
            <param name="key-lowerbound-operation.0" value="eq"/>
            <param name="key-upperbound-operation.0" value="none"/>
            <param name="write-type" value="insert"/>
            <param name="if-write-fails" value="ignore"/>
            <param name="no-match-mode" value="output-no-match-nothing"/>
            <param name="order-by-direction" value="none"/>
            <target-list>
                <item name="input" selection="all"/>
                <item name="current" prefix="b_" selection="all"/>
                <item name="old" selection="none"/>
                <item name="new" selection="none"/>
            </target-list>
            <target-list name="insert">
                <item name="input" selection="all"/>
            </target-list>
            <target-list name="update">
                <item name="input" selection="all"/>
            </target-list>
            <target-list name="no-match">
                <item name="input" selection="all"/>
            </target-list>
        </box>
        <output-stream name="DV"/>
        <box name="newPos" type="map">
            <input port="1" stream="out:GetBallPosition_1"/>
            <output port="1" stream="out:newPos_1"/>
            <target-list>
                <item name="input" selection="none">
                    <include field="b_ballId"/>
                </item>
                <expressions>
                    <declare field="ballX">b_x + (b_velocityX * Level)</declare>
                    <include field="bX">ballX</include>
                    <declare field="ballY">b_y + (b_velocityY * Level)</declare>
                    <include field="bY">ballY</include>
                </expressions>
            </target-list>
        </box>
        <box name="ReadAllBall" type="query">
            <input port="1" stream="out:Filter2_1"/>
            <output port="1" stream="out:ReadAllBall_1"/>
            <dataref id="querytable" name="BallPosition"/>
            <param name="operation" value="read"/>
            <param name="where" value="all-rows"/>
            <param name="write-type" value="insert"/>
            <param name="if-write-fails" value="ignore"/>
            <param name="no-match-mode" value="output-no-match-null"/>
            <param name="order-by-direction" value="none"/>
            <target-list>
                <item name="input" selection="all"/>
                <item name="current" selection="none">
                    <include field="ballId"/>
                </item>
                <item name="old" selection="none"/>
                <item name="new" selection="none"/>
            </target-list>
            <target-list name="insert">
                <item name="input" selection="all"/>
            </target-list>
            <target-list name="update">
                <item name="input" selection="all"/>
            </target-list>
            <target-list name="no-match">
                <item name="input" selection="all"/>
            </target-list>
        </box>
        <box name="Map2" type="map">
            <input port="1" stream="out:ReadAllBall_1"/>
            <output port="1" stream="out:Map2_1"/>
            <target-list>
                <item name="input" selection="all"/>
                <expressions>
                    <replace field="ballId">if(isnull(ballId)) then 1 else ballId</replace>
                </expressions>
            </target-list>
        </box>
        <box name="Split" type="split">
            <input port="1" stream="out:newPos_1"/>
            <output port="1" stream="out:Split_1"/>
            <output port="2" stream="out:Split_2"/>
            <param name="output-count" value="2"/>
        </box>
        <box name="CollisionBounceOrNormal" type="filter">
            <input port="1" stream="out:Split_2"/>
            <output port="1" stream="out:CollisionBounceOrNormal_1"/>
            <output port="2" stream="out:CollisionBounceOrNormal_2"/>
            <output port="3" stream="out:CollisionBounceOrNormal_3"/>
            <output port="4" stream="out:CollisionBounceOrNormal_4"/>
            <output port="5" stream="out:CollisionBounceOrNormal_5"/>
            <param name="autogen-nomatch-port" value="false"/>
            <param name="expression.0" value="bY &gt;= 590"/>
            <param name="expression.1" value="bY &lt;= 10"/>
            <param name="expression.2" value="(bY &gt;= 450 &amp;&amp; bY &lt;= 460)"/>
            <param name="expression.3" value="bX &lt;= 10 || bX &gt;= 790"/>
            <param name="expression.4" value="true"/>
        </box>
        <box name="resetAllBalls" type="query">
            <input port="1" stream="out:Map2_1"/>
            <dataref id="querytable" name="BallPosition"/>
            <param name="operation" value="write"/>
            <param name="where" value="primary-key"/>
            <param name="key-field.0" value="ballId"/>
            <param name="key-value.0" value="input1.ballId"/>
            <param name="write-type" value="update"/>
            <param name="if-write-fails" value="force"/>
            <param name="no-match-mode" value="output-no-match-null"/>
            <param name="order-by-direction" value="none"/>
            <target-list>
                <item name="input" selection="none"/>
                <item name="old" selection="none"/>
                <item name="new" selection="none"/>
                <item name="current" selection="none"/>
            </target-list>
            <target-list name="insert">
                <item name="input" selection="all"/>
            </target-list>
            <target-list name="update">
                <item name="input" selection="all"/>
                <expressions>
                    <set field="x">50</set>
                    <set field="y">50</set>
                    <declare field="velocityXrand">random()</declare>
                    <set field="velocityX">if(velocityXrand &lt; 0.5) then -5 else 5</set>
                    <declare field="velocityYrand">random()</declare>
                    <set field="velocityY">if(velocityYrand &lt; 0.5) then -5 else 5</set>
                </expressions>
            </target-list>
            <target-list name="no-match">
                <item name="input" selection="all"/>
                <expressions>
                    <set field="ballId">1</set>
                </expressions>
            </target-list>
        </box>
        <box name="storeNewPos" type="query">
            <input port="1" stream="out:Split_1"/>
            <dataref id="querytable" name="BallPosition"/>
            <param name="operation" value="write"/>
            <param name="where" value="primary-key"/>
            <param name="key-field.0" value="ballId"/>
            <param name="key-value.0" value="input1.b_ballId"/>
            <param name="write-type" value="update"/>
            <param name="if-write-fails" value="ignore"/>
            <param name="no-match-mode" value="output-no-match-null"/>
            <param name="order-by-direction" value="none"/>
            <target-list>
                <item name="input" selection="none"/>
                <item name="old" selection="none"/>
                <item name="new" selection="none"/>
                <item name="current" selection="none"/>
            </target-list>
            <target-list name="insert">
                <item name="input" selection="all"/>
            </target-list>
            <target-list name="update">
                <item name="input" selection="none"/>
                <expressions>
                    <set field="x">bX</set>
                    <set field="y">bY</set>
                    <set field="velocityX">if(bX &lt;= (10 - (velocityX * Level))) then 2
 else if (bX &gt;= (790 - (velocityX * Level))) then -2 else old.velocityX</set>
                </expressions>
            </target-list>
            <target-list name="no-match">
                <item name="input" selection="all"/>
            </target-list>
        </box>
        <box name="FetchPedal" type="query">
            <input port="1" stream="out:CollisionBounceOrNormal_3"/>
            <output port="1" stream="out:FetchPedal_1"/>
            <dataref id="querytable" name="CurrentSession"/>
            <param name="operation" value="read"/>
            <param name="where" value="expression"/>
            <param name="where-expression" value="Status == 'RUNNING'"/>
            <param name="write-type" value="insert"/>
            <param name="if-write-fails" value="ignore"/>
            <param name="no-match-mode" value="output-no-match-nothing"/>
            <param name="order-by-direction" value="none"/>
            <target-list>
                <item name="input" selection="all"/>
                <item name="current" prefix="cs_" selection="all"/>
                <item name="old" selection="none"/>
                <item name="new" selection="none"/>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="insert">
                <item name="input" selection="all"/>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="update">
                <item name="input" selection="all"/>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="no-match">
                <item name="input" selection="all"/>
            </target-list>
        </box>
        <box name="FetchSession" type="query">
            <input port="1" stream="out:CollisionBounceOrNormal_1"/>
            <output port="1" stream="out:FetchSession_1"/>
            <dataref id="querytable" name="CurrentSession"/>
            <param name="operation" value="read"/>
            <param name="where" value="secondary-key"/>
            <param name="key-field.0" value="Status"/>
            <param name="key-lowerbound-value.0" value="'RUNNING'"/>
            <param name="key-lowerbound-operation.0" value="eq"/>
            <param name="key-upperbound-operation.0" value="none"/>
            <param name="write-type" value="insert"/>
            <param name="if-write-fails" value="ignore"/>
            <param name="no-match-mode" value="output-no-match-null"/>
            <param name="order-by-direction" value="none"/>
            <target-list>
                <item name="input" selection="all"/>
                <item name="current" selection="none">
                    <include field="GameSession"/>
                </item>
                <item name="old" selection="none"/>
                <item name="new" selection="none"/>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="insert">
                <item name="input" selection="all"/>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="update">
                <item name="input" selection="all"/>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="no-match">
                <item name="input" selection="all"/>
            </target-list>
        </box>
        <box name="Split4" type="split">
            <input port="1" stream="out:CollisionBounceOrNormal_4"/>
            <output port="1" stream="out:Split4_1"/>
            <output port="2" stream="out:Split4_2"/>
            <param name="output-count" value="2"/>
        </box>
        <box name="CollisionDetection" type="filter">
            <input port="1" stream="out:FetchPedal_1"/>
            <output port="1" stream="out:CollisionDetection_1"/>
            <param name="autogen-nomatch-port" value="false"/>
            <param name="expression.0" value="(bX &gt;= (cs_PXPos - (${PaddleLength}/2)) &amp;&amp; bX &lt;= (cs_PXPos + (${PaddleLength})/2))"/>
        </box>
        <box name="CompleteData" type="outputadapter">
            <input port="1" stream="out:FetchPedal_1"/>
            <param name="start:state" value="true"/>
            <param name="javaclass" value="com.streambase.sb.adapter.csvwriter.CSVWriter"/>
            <param name="AddTimestamp" value="None"/>
            <param name="CaptureStrategy" value="FLATTEN"/>
            <param name="Charset" value=""/>
            <param name="CheckForRollAtStartup" value="false"/>
            <param name="CompressData" value="false"/>
            <param name="FieldDelimiter" value=","/>
            <param name="FileName" value="completeData.csv"/>
            <param name="FlushInterval" value="1"/>
            <param name="IfFileDoesntExist" value="Create new file"/>
            <param name="IfFileExists" value="Append to existing file"/>
            <param name="IncludeHeaderInFile" value="true"/>
            <param name="MaxFileSize" value="0"/>
            <param name="MaxRollSecs" value="0"/>
            <param name="NullValueRepresentation" value="null"/>
            <param name="OpenOutputFileDuringInit" value="false"/>
            <param name="RollHour" value="0"/>
            <param name="RollMinute" value="0"/>
            <param name="RollPeriod" value="None"/>
            <param name="RollSecond" value="0"/>
            <param name="StartControlPort" value="false"/>
            <param name="StartEventPort" value="false"/>
            <param name="StringQuoteCharacter" value="&quot;"/>
            <param name="StringQuoteOption" value="Quote if necessary"/>
            <param name="SyncOnFlush" value="false"/>
            <param name="ThrottleErrorMessages" value="false"/>
            <param name="TimestampFormat" value="yyyy-MM-dd HH:mm:ss.SSSZ"/>
            <param name="UseDefaultCharset" value="true"/>
            <param name="logLevel" value="INFO"/>
        </box>
        <box name="SessionOver" type="query">
            <input port="1" stream="out:FetchSession_1"/>
            <output port="1" stream="out:SessionOver_1"/>
            <dataref id="querytable" name="CurrentSession"/>
            <param name="operation" value="write"/>
            <param name="where" value="primary-key"/>
            <param name="key-field.0" value="GameSession"/>
            <param name="key-value.0" value="GameSession"/>
            <param name="write-type" value="update"/>
            <param name="if-write-fails" value="ignore"/>
            <param name="no-match-mode" value="output-no-match-null"/>
            <param name="order-by-direction" value="none"/>
            <target-list>
                <item name="input" selection="none"/>
                <item name="old" selection="none"/>
                <item name="new" selection="all"/>
                <item name="current" selection="none"/>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="insert">
                <item name="input" selection="all"/>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="update">
                <item name="input" selection="none"/>
                <expressions>
                    <set field="Status">'OVER'</set>
                </expressions>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="no-match">
                <item name="input" selection="all"/>
            </target-list>
        </box>
        <box name="updateVelocityX" type="query">
            <input port="1" stream="out:Split4_2"/>
            <dataref id="querytable" name="BallPosition"/>
            <param name="operation" value="write"/>
            <param name="where" value="primary-key"/>
            <param name="key-field.0" value="ballId"/>
            <param name="key-value.0" value="input1.b_ballId"/>
            <param name="write-type" value="update"/>
            <param name="if-write-fails" value="ignore"/>
            <param name="no-match-mode" value="output-no-match-null"/>
            <param name="order-by-direction" value="none"/>
            <target-list>
                <item name="input" selection="none"/>
                <item name="old" selection="none"/>
                <item name="new" selection="none"/>
                <item name="current" selection="none"/>
            </target-list>
            <target-list name="insert">
                <item name="input" selection="all"/>
            </target-list>
            <target-list name="update">
                <item name="input" selection="all"/>
                <expressions>
                    <set field="velocityX">-1 * old.velocityX</set>
                </expressions>
            </target-list>
            <target-list name="no-match">
                <item name="input" selection="all"/>
            </target-list>
        </box>
        <box name="Split3" type="split">
            <input port="1" stream="out:CollisionDetection_1"/>
            <output port="1" stream="out:Split3_1"/>
            <output port="2" stream="out:Split3_2"/>
            <output port="3" stream="out:Split3_3"/>
            <param name="output-count" value="3"/>
        </box>
        <box name="updatePosCopy" type="map">
            <input port="1" stream="out:SessionOver_1"/>
            <output port="1" stream="out:updatePosCopy_1"/>
            <target-list>
                <item name="input" selection="none"/>
                <expressions>
                    <include field="Payload">"gameover"</include>
                    <include field="Topic">"gamectrl"</include>
                    <include field="QoS">1</include>
                </expressions>
            </target-list>
        </box>
        <box name="Union" type="union">
            <input port="1" stream="out:Split3_1"/>
            <input port="2" stream="out:CollisionBounceOrNormal_2"/>
            <output port="1" stream="out:Union_1"/>
            <param name="strict" value="false"/>
        </box>
        <box name="Union3" type="union">
            <input port="1" stream="out:Split4_1"/>
            <input port="2" stream="out:Split3_2"/>
            <output port="1" stream="out:Union3_1"/>
            <param name="strict" value="false"/>
        </box>
        <box name="updateScore" type="query">
            <input port="1" stream="out:Split3_3"/>
            <output port="1" stream="out:updateScore_1"/>
            <dataref id="querytable" name="CurrentSession"/>
            <param name="operation" value="write"/>
            <param name="where" value="primary-key"/>
            <param name="key-field.0" value="GameSession"/>
            <param name="key-value.0" value="cs_GameSession"/>
            <param name="write-type" value="update"/>
            <param name="if-write-fails" value="ignore"/>
            <param name="no-match-mode" value="output-no-match-null"/>
            <param name="order-by-direction" value="none"/>
            <target-list>
                <item name="input" selection="none"/>
                <item name="old" selection="none"/>
                <item name="new" selection="none">
                    <include field="Points"/>
                </item>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="insert">
                <item name="input" selection="all"/>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="update">
                <item name="input" selection="none"/>
                <expressions>
                    <set field="Points">old.Points + 1</set>
                </expressions>
            </target-list>
            <target-list goal-schema="PlayerSessionSchema" name="no-match">
                <item name="input" selection="all"/>
            </target-list>
        </box>
        <box name="updateCanvas3" type="map">
            <input port="1" stream="out:Union3_1"/>
            <output port="1" stream="out:updateCanvas3_1"/>
            <target-list>
                <item name="input" selection="none">
                    <include field="b_ballId"/>
                    <include field="bX"/>
                    <include field="bY"/>
                </item>
            </target-list>
        </box>
        <box name="updatePosCopy2" type="map">
            <input port="1" stream="out:updateScore_1"/>
            <output port="1" stream="out:updatePosCopy2_1"/>
            <target-list>
                <item name="input" selection="none"/>
                <expressions>
                    <include field="Payload">string(Points)</include>
                    <include field="Topic">"score"</include>
                    <include field="QoS">1</include>
                </expressions>
            </target-list>
        </box>
        <box name="updateVelocityY" type="query">
            <input port="1" stream="out:Union_1"/>
            <dataref id="querytable" name="BallPosition"/>
            <param name="operation" value="write"/>
            <param name="where" value="primary-key"/>
            <param name="key-field.0" value="ballId"/>
            <param name="key-value.0" value="input1.b_ballId"/>
            <param name="write-type" value="update"/>
            <param name="if-write-fails" value="ignore"/>
            <param name="no-match-mode" value="output-no-match-null"/>
            <param name="order-by-direction" value="none"/>
            <target-list>
                <item name="input" selection="none"/>
                <item name="old" selection="none"/>
                <item name="new" selection="none"/>
                <item name="current" selection="none"/>
            </target-list>
            <target-list name="insert">
                <item name="input" selection="all"/>
            </target-list>
            <target-list name="update">
                <item name="input" selection="all"/>
                <expressions>
                    <set field="velocityY">-1 * old.velocityY</set>
                </expressions>
            </target-list>
            <target-list name="no-match">
                <item name="input" selection="all"/>
            </target-list>
        </box>
        <box name="Union2" type="union">
            <input port="1" stream="out:CollisionBounceOrNormal_5"/>
            <input port="2" stream="out:updateCanvas3_1"/>
            <output port="1" stream="out:Union2_1"/>
            <param name="strict" value="true"/>
        </box>
        <box name="updatePos" type="map">
            <input port="1" stream="out:Union2_1"/>
            <output port="1" stream="out:updatePos_1"/>
            <target-list>
                <item name="input" selection="none"/>
                <expressions>
                    <include field="Payload">"POS " + bX + " " + bY</include>
                    <include field="Topic">"ballPosition"</include>
                    <include field="QoS">1</include>
                </expressions>
            </target-list>
        </box>
        <box name="Union4" type="union">
            <input port="1" stream="out:updatePosCopy_1"/>
            <input port="2" stream="out:updatePos_1"/>
            <input port="3" stream="out:updatePosCopy2_1"/>
            <output port="1" stream="out:Union4_1"/>
            <param name="strict" value="true"/>
        </box>
        <box name="OutputAdapter" type="outputadapter">
            <input port="1" stream="out:Union4_1"/>
            <param name="start:state" value="true"/>
            <param name="javaclass" value="com.streambase.sb.adapter.mqtt.client.publish.MQTTClientPublish"/>
            <param name="CommandField" value="Command"/>
            <param name="EnableControlPort" value="false"/>
            <param name="EnableStatusPort" value="false"/>
            <param name="LogLevel" value="INFO"/>
            <param name="enableDeliveryCompleteStatusMessages" value="false"/>
            <param name="enablePendingDeliveryTokensPort" value="false"/>
            <param name="enablePublishedStatusMessages" value="false"/>
            <param name="mqttConnectionElement" value="mosquito"/>
            <param name="payloadField" value="Payload"/>
            <param name="qosField" value="Qos"/>
            <param name="retainedField" value="Retained"/>
            <param name="topicField" value="Topic"/>
        </box>
    </add>
</modify>
