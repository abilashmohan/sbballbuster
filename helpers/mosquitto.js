var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://localhost:1885');
client.on('connect', function () {
    client.subscribe("ballPosition");
    client.subscribe("gameEvent");
});

module.exports = client;