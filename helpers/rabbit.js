var context = require('rabbit.js').createContext("amqp://ping:pong@10.0.10.76");
context.on('ready', function() {
  sub = context.socket('SUB');
  sub.pipe(process.stdout);
  sub.connect('ball', function() {
      console.log("Connected...");
  });
});

module.exports.context = context;