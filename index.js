var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
//var rabbit = require("./helpers/rabbit");
var mosquitto = require("./helpers/mosquitto");
app.use(express.static("view"));

server.listen(3001);

io.on('connection', function (socket) {
    var player = null;
    socket.on('register', function (data) {
        if (data !== null) {
            //there was something in localstorage
           console.log("somethine came" + data);
           mosquitto.publish("reload",data);
        } else {
            //localStorage is not set, create new player
        }
    });
    socket.emit('news', { hello: 'world' });
    socket.on('paddle', function (data) {
        // console.log("Paddle ", data);
        mosquitto.publish("paddle", String(data));
    });
});
    
mosquitto.on("message", function (topic, message) {
    if (topic === "ballPosition") {
        var messageArr = message.toString().split(" ");
        var msg = {
            x: messageArr[1],
            y: messageArr[2]
        };
        io.emit("ball", msg);
    } else {
        console.log("Unknown Topic ", topic);
}
});



