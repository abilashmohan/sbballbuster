
console.log("Loading the game");

var game = new Phaser.Game(800, 600, Phaser.AUTO, "canvas", { preload: preload, create: create, update:update});
var ball = null;
function preload () {
    game.load.spritesheet('ball', 'assets/ui_ball.png',64,64,7);
    Paddle.preload(game);
}

function create () {
    ball = game.add.sprite(game.world.centerX, game.world.centerY, 'ball');
    var ballSpin = ball.animations.add('spin');
    ball.animations.play('spin', 15, true);
    ball.anchor.setTo(0.5, 0.5);
    Paddle.create(game);
}

function update() {
    ball.x = BallEngine.x;
    ball.y = BallEngine.y;
    Paddle.update(game);
    socket.emit('paddle', Paddle.paddle.x);
}