var Paddle = {
    paddle: null,
    animationRunning: false,
    preload: function (game) {
        // game.load.image("paddle", "assets/paddle2.png");
        game.load.spritesheet("paddle", "assets/scottpilgrim.png", 108, 140,16);
    },
    create: function (game) {
        //game.world.ceneterX
        this.paddle = game.add.sprite(300, game.height * 0.9, "paddle");
        this.paddle.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enableBody(this.paddle);
        this.paddle.body.collideWorldBounds = true;
        this.paddle.body.immovable = true;
        //this.paddle.body.bounce.setTo(0.2, 0.2);
        this.cursors = game.input.keyboard.createCursorKeys();
        this.leftAnimation = this.paddle.animations.add("left", [8, 9, 10, 11, 12, 13, 14, 15], 15, true);
        this.rightAnimation = this.paddle.animations.add("right", [0, 1, 2, 3, 4, 5, 6, 7], 15, true);
    },
    facingLeft : false,
    update: function (game) {
        
        if (this.cursors.left.isDown) {
            this.facingLeft = true;
            this.paddle.body.velocity.x = -500;
        } else if (this.cursors.right.isDown) {
            this.facingLeft = false;
            this.paddle.body.velocity.x = 500;
        } else {
            if(this.paddle.body.velocity.x > 0){
                this.paddle.body.velocity.x -= 10;
               
            } else if(this.paddle.body.velocity.x < 0){
                this.paddle.body.velocity.x += 10;
      
            }
        }
        if (this.paddle.body.velocity.x > 0 ) {
            console.log("Running Right...");
            this.paddle.animations.play("right");
        } else if (this.paddle.body.velocity.x < 0) {
            console.log("Running Left...");
            this.paddle.animations.play("left");
        } else {
            this.paddle.animations.stop();
            this.paddle.frame = this.facingLeft ? 11 : 4;
        }
    }
}