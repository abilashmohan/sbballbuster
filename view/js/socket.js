var socket = io('http://localhost:3001');
var randomUser = Math.random().toString(36).substring(3,16)
socket.emit('register', randomUser);

socket.on('news', function (data) {
  socket.emit('my other event', { my: 'player' });
});
socket.on("ball", function (msg) {
  BallEngine.update(msg.x, msg.y);
});
